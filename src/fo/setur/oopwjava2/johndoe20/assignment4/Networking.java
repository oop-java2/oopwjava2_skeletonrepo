package fo.setur.oopwjava2.johndoe20.assignment4;

public class Networking {
	public static final String BROADCASTSTRING = "<udp><broadcast><senderip>%s</senderip><senderport>%d</senderport>"
			                                   + "<username>%s</username><broadcastmask>%s</broadcastmask><time>%d</time></broadcast></udp>";
	public static final int BROADCASTPORT = 50404; // The port the clients are listening on.
	public static final String BROADCASTMASK = "230.0.0.0";
	public static final int BROADCASTINTERVAL = 999;
	public static final String MESSAGESTRING = "<udp><message><messageid>%s</messageid><senderip>%s</senderip><senderport>%d</senderport><sender>%s</sender><recipient>%s</recipient>"
			 							     + "<content>%s</content><broadcastmask>%s</broadcastmask><time>%d</time></message></udp>";
	


	public static String buildMulticastMessage(String ip, int senderport,String username, String broadcastmask, long currentTimeMillis) {
		return String.format(BROADCASTSTRING,ip,senderport,username,broadcastmask,currentTimeMillis);
	}

	public static String buildMessage(String messageID, String localIP, int broadcastport, String username,String recipient,String message,String broadcastmask, long currentTimeMillis) {
		return String.format(MESSAGESTRING,messageID,localIP,broadcastport,username,recipient,message,broadcastmask,currentTimeMillis);
	}
}
