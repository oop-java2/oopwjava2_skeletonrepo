package fo.setur.oopwjava2.johndoe20.assignment4;


public class StringParse {
	
	/**
	 * Returns the content of a tag, e.g. text in <tag>text</tag>.
	 * @param raw
	 * @param tag
	 * @return
	 */
	public static String getTagContent(String raw, String tag) {
		String startTag = "<"+tag+">";
		String stopTag  = "</"+tag+">";
		int i = raw.indexOf(startTag);
		int j = raw.indexOf(stopTag);
		if (i>-1 && j>-1) {
			return raw.substring(i+startTag.length(), j);
		}
		else return "";
	}

}
