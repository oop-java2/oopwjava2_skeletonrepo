package fo.setur.oopwjava2.johndoe20.assignment4;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class NetworkingUtil {
	/**
	 * List all Network interfaces and all IP addresses of all Network addresses.
	 */
	public static void listNetworkInterfaces() {
		try {
			Enumeration<NetworkInterface> ns = NetworkInterface.getNetworkInterfaces();
			while (ns.hasMoreElements()  ) {
			  NetworkInterface n = ns.nextElement();
			  Enumeration<InetAddress> ni = n.getInetAddresses();
			  System.out.printf("Network Interface: %s ...\n",n.getName());
			  while (ni.hasMoreElements()) {
				  InetAddress ia = ni.nextElement();
				  String ip = ia.getHostAddress();
				  System.out.println("IP: "+ip);
			  }
			}
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
	}
	

}
