package fo.setur.oopwjava2.johndoe20.assignment5.jarih.exercise07_38;
//Exercise 7.38 Solution: SimulatorTest.java
//Modified by Jari í Hjøllum, fall 2021, from 
// Exercise 7.37 Solution: SimulatorTest.java
// Test application for class Simulator
public class SimulatorTest{
	public static final String path = "src/fo/setur/oopwjava2/johndoe20/assignment5/jarih/exercise07_38/";
	public static final String filename = "test_exp.sml";
   public static void main(String[] args) {
	   
	  TextFileLoader textFileLoader = new TextFileLoader(path,filename);
	  String smlProgram = textFileLoader.getFileContent();
	  System.out.println(smlProgram);

	  Simulator simpletron = new Simulator();
      // initialize the registers
      simpletron.initializeRegisters();
      
      // prompt the user to enter instructions
//      simpletron.printInstructions();
//      simpletron.loadInstructions();
      simpletron.loadProgram(smlProgram);
      
      // execute the program and print the memory dump when finished
      simpletron.execute();
      simpletron.dump();
   }    
}


/**************************************************************************
 * (C) Copyright 1992-2018 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
