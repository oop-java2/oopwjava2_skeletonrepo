package fo.setur.oopwjava2.johndoe20.assignment5;

public class Instruction {
   public static final int READ = 10;
   public static final int WRITE = 11;
   public static final int LOAD = 20;
   public static final int STORE = 21;
   public static final int ADD = 30;
   public static final int SUBTRACT = 31;
   public static final int MULTIPLY = 32;
   public static final int DIVIDE = 33;
   public static final int REMAINDER = 34;
   public static final int EXP = 35;
   public static final int BRANCH = 40; 
   public static final int BRANCH_NEG = 41;
   public static final int BRANCH_ZERO = 42;
   public static final int HALT = 43;

}
