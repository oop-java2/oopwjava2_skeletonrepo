5 rem Program to algebraic expressions
10 input x
20 input y
30 input z
30 let w = x + y
40 print w
50 let w = x + y + z
60 print w
70 let w = x - y
80 print w
90 let w = x * y
100 print w
110 let w = x / y
120 print w
130 let w = (x + y) * z
140 print w
150 let w = x^y
160 print w
170 let w = x % y
180 print w
999 end
