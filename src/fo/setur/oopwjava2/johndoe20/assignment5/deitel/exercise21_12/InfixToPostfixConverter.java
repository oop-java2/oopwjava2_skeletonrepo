package fo.setur.oopwjava2.johndoe20.assignment5.deitel.exercise21_12;
// Exercise 21.12 Solution: InfixToPostfixConverter.java
// Infix to postfix conversion
import java.util.Scanner;

public class InfixToPostfixConverter {
   public static void main(String[] args) {
      // get infix expression
      Scanner scanner = new Scanner(System.in);
      System.out.println("Please enter an infix expression:");
      StringBuffer infix = new StringBuffer(scanner.nextLine());
      System.out.printf(
         "\nThe original infix expression is:\n%s\n", infix);

      // change from infix notation into postfix notation
      StringBuffer postfix = convertToPostfix(infix);
   
      System.out.printf(
         "The expression in postfix notation is:\n%s\n", postfix);
   }   

   // take out the infix and change it into postfix
   public static StringBuffer convertToPostfix(StringBuffer infix) {
      Stack<Character> charStack = new Stack<>();
      StringBuffer temporary = new StringBuffer("");

      // push a left paren onto the stack and add a right paren to infix
      charStack.push('(');
      infix.append(')');

      // convert the infix expression to postfix
      for (int infixCount = 0; !charStack.isEmpty(); ++infixCount) {
         if (Character.isDigit(infix.charAt(infixCount))) {
            temporary.append(infix.charAt(infixCount) + " ");
         }
         else if (infix.charAt(infixCount) == '(') {
            charStack.push('(');
         }
         else if (isOperator(infix.charAt(infixCount))) {
            while (isOperator(charStack.peek()) && 
               precedence(infix.charAt(infixCount), charStack.peek())) {
               temporary.append(charStack.pop() + " ");
            }

            charStack.push(infix.charAt(infixCount));
         }
         else if (infix.charAt(infixCount) == ')') {
            while (charStack.peek() != '(') {
               temporary.append(charStack.pop() + " ");
            }

            charStack.pop();
         }
      }

      return temporary;
   }

   // check if c is an operator
   private static boolean isOperator(char c) {
      return c == '+' || c == '-' || c == '*' ||
         c == '/' || c == '^' || c == '%';
   }

   // true if operator1 has precedence less than operator2
   private static boolean precedence(char operator1, char operator2) {
      if (operator1 == '^') {
         return false;
      }
      else if (operator2 == '^') {
         return true;
      }
      else if (operator1 == '*' || operator1 == '/' || operator1 == '%') {
         return false;
      }
      else if (operator2 == '*' || operator2 == '/' || operator2 == '%') {
         return true;
      }
      else {
         return false;
      }
   }
}


/**************************************************************************
 * (C) Copyright 1992-2018 by Deitel & Associates, Inc. and               *
 * Prentice Hall. All Rights Reserved.                                    *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
