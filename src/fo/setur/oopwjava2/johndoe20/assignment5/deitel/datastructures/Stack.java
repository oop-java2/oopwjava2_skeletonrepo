package fo.setur.oopwjava2.johndoe20.assignment5.deitel.datastructures;
// Exercise 21.12 Solution: Stack.java
// Stack uses a composed List object.
// Difference from chapter: added peek method.
import java.util.NoSuchElementException;

public class Stack<T> {
   private final List<T> stackList;

   // no-argument constructor
   public Stack() { 
      stackList = new List<>("stack"); 
   }

   // add object to stack
   public void push(T object) { 
      stackList.insertAtFront(object); 
   }

   // remove object from stack
   public T pop() throws NoSuchElementException { 
      return stackList.removeFromFront(); 
   }

   public T peek() throws NoSuchElementException {
      T temp = pop(); // remove from stack
      push(temp); // push back on to stack
      return temp; // return value
   }

   // determine if stack is empty
   public boolean isEmpty() { 
      return stackList.isEmpty(); 
   }

   // output stack contents
   public void print() { 
      stackList.print(); 
   }
}


/**************************************************************************
 * (C) Copyright 1992-2018 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
