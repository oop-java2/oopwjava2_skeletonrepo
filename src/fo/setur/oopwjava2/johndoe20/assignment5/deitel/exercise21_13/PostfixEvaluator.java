package fo.setur.oopwjava2.johndoe20.assignment5.deitel.exercise21_13;
// Exercise 21.13 Solution: PostfixEvaluator.java
// Using a stack to evaluate an expression in postfix notation
import java.util.Scanner;
import com.deitel.datastructures.Stack;

public class PostfixEvaluator {
   public static void main(String[] args) {
      // get postfix expression
      Scanner scanner = new Scanner(System.in);
      System.out.println("Please enter a postfix expression:");
      StringBuffer postfix = new StringBuffer(scanner.nextLine());
      System.out.printf(
         "\nThe original postfix expression is:\n%s\n", postfix);

      // evaluate postfix expression
      int answer = evaluatePostfixExpression(postfix);
   
      System.out.printf("The value of the expression is: %d\n", answer);
   }

   // evaluate the postfix notation
   public static int evaluatePostfixExpression(StringBuffer expr) {
      Stack<Integer> intStack = new Stack<>();
      
      expr.append(")");
   
      // until it reaches ")"
      for (int i = 0; expr.charAt(i) != ')'; ++i) {
         if (Character.isDigit(expr.charAt(i))) {
            int pushVal = expr.charAt(i) - '0';
            intStack.push(pushVal);
            intStack.print();
         }
         else if (!Character.isWhitespace(expr.charAt(i))) {
            int popVal2 = intStack.pop();
            intStack.print();
            int popVal1 = intStack.pop();
            intStack.print();
            int pushVal = calculate(popVal1, popVal2, expr.charAt(i));
            intStack.push(pushVal);
            intStack.print();
         }
      }

      return intStack.pop();
   }

   // do the calculation
   private static int calculate(int op1, int op2, char oper) {
      switch(oper) {
         case '+':
            return op1 + op2;
         case '-':
            return op1 - op2;
         case '*':
            return op1 * op2;
         case '/':
            return op1 / op2;
         case '%':
            return op1 % op2;
         case '^':   // exponentiation
            return (int)Math.pow(op1, op2);
      }

      return 0;
   } 
}


/**************************************************************************
 * (C) Copyright 1992-2018 by Deitel & Associates, Inc. and               *
 * Prentice Hall. All Rights Reserved.                                    *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
