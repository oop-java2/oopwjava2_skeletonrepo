package fo.setur.oopwjava2.johndoe20.assignment5;

public class FilenameUtil {
	
	/**
	 * Returns the original filename with the new extension. If no extension is 
	 * found, the new extension is added.
	 * @param filename original filename.
	 * @param newExtension including ., e.g. ".txt"
	 * @return
	 */
	public static String changeExt(String filename, String newExtension) {
		int index = filename.lastIndexOf('.');
		if (index>-1) {
			filename = filename.substring(0, index)+newExtension;
		}
		else {
			filename += newExtension;
		}
		
		return filename;
	}

}
