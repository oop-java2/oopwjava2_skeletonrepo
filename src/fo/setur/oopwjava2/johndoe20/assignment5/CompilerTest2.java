package fo.setur.oopwjava2.johndoe20.assignment5;

public class CompilerTest2 {
	public static String path = "src/fo/setur/oopwjava2/johndoe20/assignment5/tests/";
	public static String srcfile = path+"fig2130.bas";
//	public static String srcfile = path+"test_assign.bas"; // 
//	public static String srcfile = path+"test_eq.bas"; //
//	public static String srcfile = path+"test_neq.bas"; //
//	public static String srcfile = path+"test_gt.bas"; //
//	public static String srcfile = path+"test_lt.bas"; //
//	public static String srcfile = path+"test_lte.bas"; //
//	public static String srcfile = path+"test_gte.bas"; //
//	public static String srcfile = path+"test_largeint.bas"; //
//	public static String srcfile = path+"test_algebra.bas"; //
	public static String smlfile = FilenameUtil.changeExt(srcfile, ".sml");

	public static void main(String[] args) {
		Compiler compiler = new Compiler();
		compiler.loadProgramFile(srcfile);
		compiler.compile(smlfile);
		
		Simulator simulator = new Simulator();
		simulator.loadFileProgram(smlfile);
		simulator.execute();
		simulator.dump();
	}

}
