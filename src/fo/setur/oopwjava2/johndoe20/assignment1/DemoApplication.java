package fo.setur.oopwjava2.johndoe20.assignment1;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class DemoApplication extends Application{
	
	public static void main(String[] args) {
		Application.launch(DemoApplication.class, args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("DemoApplication.fxml"));
		Scene scene = new Scene(root);
		primaryStage.setTitle("DemoApplication by John Doe");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

}